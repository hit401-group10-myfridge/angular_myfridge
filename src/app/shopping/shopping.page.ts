import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../app.component';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.page.html',
  styleUrls: ['./shopping.page.scss'],
})
export class ShoppingPage implements OnInit {

  public product = [];
  name:string;
  email:string;
  productName:string;
  productAmount:string;
  protectCatagory:string;
  public productNamesArray = [];
  public productAmountArray = [];
  public productCatArray = [];
  
  constructor(private appcomponent:AppComponent,private route: ActivatedRoute,private router: Router) {}
  ngOnInit(){

    this.name = localStorage.getItem('name');
    this.email = localStorage.getItem('email');
    document.querySelector('.item_name').innerHTML = `<table border='1'>
    <body>
    <tr>
    <td>Names: </td><td>${JSON.parse(localStorage.getItem('names'))}</td> 
    </tr>
    <br>
    <tr>
    <td>Amounts: </td><td>${JSON.parse(localStorage.getItem('amts'))}</td>
    </tr>
    <br>
    <tr>
    <td>Categories: </td><td>${JSON.parse(localStorage.getItem('categs'))}</td>
    </tr>
    </tbody>
    </table>`;
    console.log(JSON.parse(localStorage.getItem('names')));
    console.log(JSON.parse(localStorage.getItem('amts')));
    console.log(JSON.parse(localStorage.getItem('categs')));
    // JSON.parse(localStorage.getItem('amounts'));
    // JSON.parse(localStorage.getItem('categs'));
    if(this.email=="" || this.email==undefined){
      this.router.navigate(['login']);
    }

    

  }

  onClickSubmit(data){
    console.log(data);
    this.productName=data.name;
    this.productAmount=data.amount;
    this.protectCatagory=data.catagory;
    console.log("Name entered: " + data.name);
    console.log("Amt entered: " + data.amount);
    console.log("Categ entered: " + data.catagory);
    this.productNamesArray.push(data.name);
    this.productAmountArray.push(data.amount);
    this.productCatArray.push(data.catagory);
    console.log("Names Array: " +this.productNamesArray);
    console.log("Amt Array: " +this.productAmountArray);
    console.log("Cat Array: " +this.productCatArray);

    
    const out = document.querySelector('.item_name');
    console.log(out);
    var names = localStorage.setItem("names", JSON.stringify(this.productNamesArray));
    var amts = localStorage.setItem("amts", JSON.stringify(this.productAmountArray));
    var categs = localStorage.setItem("categs", JSON.stringify(this.productCatArray));

  }


  inventory(){
    this.router.navigate(['inventory']);
  }
  scan(){
    this.router.navigate(['scanitem']);
  }
  home(){
    this.router.navigate(['home']);
  }
  recipe(){
    this.router.navigate(['receipt']);
  }
  shopping(){
    this.router.navigate(['home']);
  }
  profile(){
    this.router.navigate(['profile']);
  }

}
