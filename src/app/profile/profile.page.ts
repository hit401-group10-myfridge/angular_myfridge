import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../app.component';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  name:string;
  email:string;
  
  constructor(private appcomponent:AppComponent,private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
    this.email = localStorage.getItem('email');
    if(this.email=="" || this.email==undefined){
      this.router.navigate(['login']);
    }

    
  }
  signup(){
    this.router.navigate(['login']);
  }

}
