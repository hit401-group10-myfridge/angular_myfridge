import { Component, OnInit } from '@angular/core';
//import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';
import {AppComponent} from '../app.component';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email:string;
  password:string;

  constructor(private appcomponent:AppComponent,private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
  }
  onClickSubmit(data){
    this.email="";
    this.password="";
    console.log(data);
    if(data.email=="" || data.email==null){
      this.email="Enter an email";
    }else if(data.password=="" || data.password==null){
      this.password="Enter an password";
    }else{
      var emailid = localStorage.getItem('email');
      var passworddata = localStorage.getItem('password');
      if(emailid==data.email){
        if(passworddata==data.password){
          this.router.navigate(['home']);
        }else{
          this.password="Password Not Match";
        }
      }else{
        this.email="Email Not Match";
      }

    }
  }

}
