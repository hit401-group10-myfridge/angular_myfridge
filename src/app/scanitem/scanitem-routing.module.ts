import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScanitemPage } from './scanitem.page';

const routes: Routes = [
  {
    path: '',
    component: ScanitemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScanitemPageRoutingModule {}
