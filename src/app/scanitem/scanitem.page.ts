import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../app.component';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from "@ionic-native/barcode-scanner/ngx";


@Component({
  selector: 'app-scanitem',
  templateUrl: './scanitem.page.html',
  styleUrls: ['./scanitem.page.scss'],
})
export class ScanitemPage implements OnInit {

  encodedData: any;
  scannedBarCode: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  name:string;
  email:string;
  constructor(private appcomponent:AppComponent,private route: ActivatedRoute,private router: Router, private scanner: BarcodeScanner) { 

    this.encodedData = "Programming isnt' about what you know";

    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }

  ngOnInit() {
    this.name = localStorage.getItem('name');
    this.email = localStorage.getItem('email');
    if(this.email=="" || this.email==undefined){
      this.router.navigate(['login']);
    }    

    document.getElementById("home").style.display="block";
  };

  scanBRcode() {
    this.scanner.scan().then(res => {
      this.scannedBarCode = res;
    }).catch(err => {
      alert(err);
    });
  }
  generateBarCode() {
    this.scanner.encode(this.scanner.Encode.TEXT_TYPE, this.encodedData).then(
      res => {
        alert(res);
        this.encodedData = res;
      }, error => {
        alert(error);
      }
    );
  }

  checkitem(){
    console.log("Check");
    document.getElementById("home").style.display="none";
    document.getElementById("checkItem").style.display="block";
    document.getElementById("checkItemdata").style.display="block";
  }
  back1(){
     document.getElementById("home").style.display="block";
    //  checkItem
    document.getElementById("checkItemdata").style.display="none";
    document.getElementById("checkItem").style.display="none";
  }

  inventory(){
    this.router.navigate(['inventory']);
  }
  scan(){
    this.router.navigate(['scanitem']);
  }
  home(){
    this.router.navigate(['home']);
  }
  recipe(){
    this.router.navigate(['receipt']);
  }
  shopping(){
    this.router.navigate(['home']);
  }
  profile(){
    this.router.navigate(['profile']);
  }

  addrecipes(){
    this.router.navigate(['shopping']);
  }
  }
