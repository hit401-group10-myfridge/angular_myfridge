import { Component } from '@angular/core';
import {AppComponent} from '../app.component';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public product = [];
  name:string;
  email:string;
  constructor(private appcomponent:AppComponent,private route: ActivatedRoute,private router: Router) {}
  ngOnInit(){

    this.name = localStorage.getItem('name');
    this.email = localStorage.getItem('email');
    console.log(this.email);
    if(this.email=="" || this.email==undefined){
      this.router.navigate(['login']);
    }
    this.product =[
      {
        ProductName :"White bread",
        ProductDescription:"White bread typically refers to breads made from wheat flour from which the bran and the germ layers have been removed (and set aside) from the whole .",
        PriductImg:"assets/bread.jpg",
        Price:4,
      },
      {
        ProductName :"Pepsi 600ml",
        ProductDescription:"Pepsi Soft Drink · About the Product. Pepsi Soft Drink 600 ml · Nutritional Facts. Typical value per 100 ml of product. Energy (kcal) - 43. Protein (g) - 0",
        PriductImg:"assets/pespi.jpg",
        Price:3,
      },
      {
        ProductName :"Carrot Prepacked(1kg)",
        ProductDescription:"Carrot, (Daucus carota), herbaceous, generally biennial plant of the Apiaceae family that produces an edible taproot, white-, yellow-, and purple",
        PriductImg:"assets/Carrot.jpg",
        Price:2,
      },
      {
        ProductName :"Cookies",
        ProductDescription:"A cookie is a baked or cooked snack or dessert that is typically small, flat and sweet. It usually contains flour, sugar, egg, and some type of oil, fat, or butter.",
        PriductImg:"assets/Cookies.jpg",
        Price:10,
      },

      {
        ProductName :"White bread(1kg)",
        ProductDescription:"White bread typically refers to breads made from wheat flour from which the bran and the germ layers have been removed (and set aside) from the whole .",
        PriductImg:"assets/bread.jpg",
        Price:8,
      },
      {
        ProductName :"Pepsi 900ml",
        ProductDescription:"Pepsi Soft Drink · About the Product. Pepsi Soft Drink 600 ml · Nutritional Facts. Typical value per 100 ml of product. Energy (kcal) - 43. Protein (g) - 0",
        PriductImg:"assets/pespi.jpg",
        Price:4,
      },
      {
        ProductName :"Carrot Prepacked(2kg)",
        ProductDescription:"Carrot, (Daucus carota), herbaceous, generally biennial plant of the Apiaceae family that produces an edible taproot, white-, yellow-, and purple",
        PriductImg:"assets/Carrot.jpg",
        Price:4,
      },
      {
        ProductName :"Cookies 250gm",
        ProductDescription:"A cookie is a baked or cooked snack or dessert that is typically small, flat and sweet. It usually contains flour, sugar, egg, and some type of oil, fat, or butter.",
        PriductImg:"assets/Cookies.jpg",
        Price:12,
      },

      
    ]


  }


  inventory(){
    this.router.navigate(['inventory']);
  }
  scan(){
    this.router.navigate(['scanitem']);
  }
  home(){
    this.router.navigate(['home']);
  }
  recipe(){
    this.router.navigate(['receipt']);
  }
  shopping(){
    this.router.navigate(['home']);
  }
  profile(){
    this.router.navigate(['profile']);
  }


}
