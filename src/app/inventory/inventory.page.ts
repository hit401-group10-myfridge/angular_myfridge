import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../app.component';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';
import { ShoppingPage } from '../shopping/shopping.page';



@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.page.html',
  styleUrls: ['./inventory.page.scss'],
})
export class InventoryPage implements OnInit {
  public product = [];
  name:string;
  email:string;
  constructor(private appcomponent:AppComponent,private route: ActivatedRoute,private router: Router) { }
  
  ngOnInit() {
    this.name = localStorage.getItem('name');
    this.email = localStorage.getItem('email');
    console.log(ShoppingPage.prototype.productAmountArray);
    console.log(this.email);
    if(this.email=="" || this.email==undefined){
      this.router.navigate(['login']);
    }
    this.product =[
      {
        ProductName:"White Bread",
        Quantity:"2"
      },
      {
        ProductName:"Free range eggs",
        Quantity:"12"
      },
      {
        ProductName:"Full cream milk",
        Quantity:"1 litre"
      },
      {
        ProductName:"Pepsi",
        Quantity:"28"
      },
      {
        ProductName:"Carrot",
        Quantity:"0"
      },
      {
        ProductName:"Cookies",
        Quantity:"0"
      },
    ]
  }
  inventory(){
    this.router.navigate(['inventory']);
  }
  scan(){
    this.router.navigate(['scanitem']);
  }
  home(){
    this.router.navigate(['home']);
  }
  recipe(){
    this.router.navigate(['receipt']);
  }
  shopping(){
    this.router.navigate(['home']);
  }
  profile(){
    this.router.navigate(['profile']);
  }

}
