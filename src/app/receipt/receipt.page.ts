import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../app.component';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.page.html',
  styleUrls: ['./receipt.page.scss'],
})
export class ReceiptPage implements OnInit {
  name:string;
  email:string;
  constructor(private appcomponent:AppComponent,private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
    this.email = localStorage.getItem('email');
    if(this.email=="" || this.email==undefined){
      this.router.navigate(['login']);
    }
  }

  inventory(){
    this.router.navigate(['inventory']);
  }
  scan(){
    this.router.navigate(['scanitem']);
  }
  home(){
    this.router.navigate(['home']);
  }
  recipe(){
    this.router.navigate(['receipt']);
  }
  shopping(){
    this.router.navigate(['home']);
  }
  profile(){
    this.router.navigate(['profile']);
  }


  addrecipes(){
    this.router.navigate(['shopping']);
  }
}
