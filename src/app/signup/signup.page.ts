import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../app.component';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  email:string;
  name:string;
  password:string;
  cpassword:string;
  constructor(private appcomponent:AppComponent,private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
  }
  onClickSubmit(data){
    this.email="";
    this.name="";
    this.cpassword="";
    this.password="";
    console.log(data);
    if(data.name=="" || data.name==null){
      this.name="fill the name";
    }else if(data.email=="" || data.email==null){
      this.email="fill the email";
    }else if(data.password=="" || data.password==null){
      this.password="fill the password";
    }else if(data.cpassword=="" || data.cpassword==null){
      this.cpassword="fill the Confirm Password";
    }else if(data.password==data.cpassword){
      localStorage.setItem("email",data.email);
      localStorage.setItem("name",data.name);
      localStorage.setItem("password",data.password);
      this.appcomponent.ngOnInit();
      this.router.navigate(['login']);
    }else{
      this.cpassword="Password not match";
    }
  }

}
